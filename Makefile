CC = gcc
CFLAGS = -Wall -g -Iinclude

SRC_DIR = src
BUILD_DIR = build
OBJ_DIR = $(BUILD_DIR)/obj

OBJECTS = $(OBJ_DIR)/graph.o \
	$(OBJ_DIR)/util.o \
	$(OBJ_DIR)/heuristic.o \
	$(OBJ_DIR)/maxflow.o

all: setup sp2020

setup:
	mkdir -p $(OBJ_DIR)

clean:
	rm -rf $(BUILD_DIR)

sp2020: $(SRC_DIR)/main.c $(OBJECTS)
	$(CC) $(CFLAGS) -o $(BUILD_DIR)/$@ $< $(OBJECTS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -o $@ -c $<
