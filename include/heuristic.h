/*
 * heuristic.h - heuristics for perfect elimination orders
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEURISTIC_H
#define HEURISTIC_H

#include "graph.h"
#include "util.h"

struct SearchHeuristic
{
  int (*greater_than) (void *a, void *b);
  void (*keys_destroy) (void **keys, unsigned size);
  void **(*keys_init) (unsigned size);
  void (*search_next) (struct Graph const *graph, struct MaxHeap *unreached,
                       struct List *elimination_order, void **keys);
};

struct SearchHeuristicData
{
  struct SearchHeuristic heuristic;
  struct Graph *graph;
  void **keys;
  struct MaxHeap *unreached;
  struct List *elimination_order;

  // search heuristics first add nodes to this. They are pushed to the
  // back of elimination_order in reverse order at the end.
  struct List *elimination_order_reverse;
};

extern const struct SearchHeuristic lexbfs;
extern const struct SearchHeuristic max_cardinality_search;

struct List*
min_separator (struct Graph const *graph, unsigned source, unsigned target);

struct FlowNetwork*
min_separator_flow_network_init (struct Graph const *graph, unsigned source,
                                 unsigned target);

void
msvs (struct Graph const *graph, struct TreeDecomp *td);

void
reduce_almost_simplicial (struct SearchHeuristicData *data);

void
reduce_buddy (struct SearchHeuristicData *data);

void
reduce_simplicial (struct SearchHeuristicData *data);

/*
 * Use this when you don't want to use reduction rules or as a reference
 * implementation of how to use search_heuristic_run_on
 */
struct List*
search_heuristic_run (struct SearchHeuristic heuristic,
                      struct Graph const *graph);

/* Use this when you want to use reduction rules */
void
search_heuristic_run_on (struct SearchHeuristicData *data);

void
search_heuristic_data_destroy (struct SearchHeuristicData *data);

struct SearchHeuristicData*
search_heuristic_data_init (struct SearchHeuristic heuristic,
                            struct Graph const *graph);

unsigned
tree_width_lower_bound (struct Graph const *graph);

#endif
