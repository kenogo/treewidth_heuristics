/*
 * maxflow.h - calculate the maximum flow of a flow network
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAXFLOW_H
#define MAXFLOW_H

#include "graph.h"
#include "util.h"

struct FlowNetwork
{
  struct DirectedGraph *graph;
  // capacities of the edges; same indexing as graph->neighbors_in and
  // graph->neighbors_out, respectively
  struct List **cap_in;
  struct List **cap_out;
  unsigned source;
  unsigned target;
};

struct Flow
{
  struct FlowNetwork const *network;
  // same indexing as network->graph->neighbors_in and
  // network->graph->neighbors_out, respecitvely
  struct List **in;
  struct List **out;
};

struct ResidualNetwork
{
  struct Flow const *flow;
  struct DirectedGraph *graph;
  struct List **cap_in;
  struct List **cap_out;
  struct List **reverse_cap_in;
  struct List **reverse_cap_out;
  unsigned source;
  unsigned target;
};

void
flow_destroy (struct Flow *flow);

struct Flow *
flow_init (struct FlowNetwork const *network);

void
flow_network_destroy (struct FlowNetwork *flow_network);

struct Flow *
maxflow (struct FlowNetwork *network);

void
max_path_flow (struct ResidualNetwork *residual, struct List const *path);

void
residual_network_destroy (struct ResidualNetwork *residual);

struct ResidualNetwork *
residual_network_init (struct Flow const *flow);

void
residual_network_rebuild (struct ResidualNetwork *residual);

struct List *
residual_network_search_path (struct ResidualNetwork *residual);

#endif
