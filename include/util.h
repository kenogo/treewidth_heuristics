/*
 * util.h - utilties like lists and heaps
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H
#define UTIL_H

#define LIST_INITIAL_CAPACITY 64
#define MAX_HEAP_INITIAL_CAPACITY 64
#define QUEUE_INITIAL_CAPACITY 64

struct List
{
  unsigned *elements;
  unsigned size;
  unsigned cap;
};

struct MaxHeap
{
  void **keys_ptrs; // Array of pointers to the actual keys
  int (*greater_than) (void* a, void* b); // Return if *a > *b
  unsigned *values;
  unsigned size;
  unsigned cap;
};

struct Queue
{
  unsigned *elements;
  // Position of the start of the queue
  unsigned current_pos;
  unsigned size;
  unsigned cap;
};

void
list_append (struct List *list1, struct List const *list2);

void
list_copy (struct List *target, struct List const *source);

void
list_destroy (struct List *list);

int
list_find (struct List const *list, unsigned value);

unsigned
list_get (struct List const *list, unsigned pos);

struct List *
list_get_list (struct List const *list, struct List const *indices);

struct List*
list_init ();

unsigned
list_max (struct List const *list);

unsigned
list_push_back (struct List *list, unsigned value);

int
list_remove (struct List *list, unsigned element);

int
list_remove_at (struct List *list, unsigned pos);

void
list_reverse (struct List *list);

void
list_set (struct List *list, unsigned pos, unsigned value);

int
readline (char **buf, size_t *size, FILE *fp);

void
max_heap_destroy (struct MaxHeap *heap);

int
max_heap_extract (struct MaxHeap *heap);

struct MaxHeap*
max_heap_init (int (*greater_than) (void* a, void *b));

void
max_heap_insert (struct MaxHeap *heap, void *key_ptr, unsigned value);

void
max_heap_rebuild (struct MaxHeap *heap);

void
max_heap_sift_down (struct MaxHeap *heap, unsigned pos);

void
max_heap_sift_up (struct MaxHeap *heap, unsigned pos);

int
queue_dequeue (struct Queue *queue);

void
queue_destroy (struct Queue *queue);

void
queue_enqueue (struct Queue *queue, unsigned value);

struct Queue *
queue_init ();

int
set_add (struct List *set, unsigned value);

int
set_equal (struct List const *set1, struct List const *set2);

int
set_intersects (struct List const *set1, struct List const *set2);

int
set_is_superset (struct List const *set1, struct List const *set2);

unsigned
set_substract (struct List *set1, struct List const *set2);

unsigned
set_union (struct List *set1, struct List const *set2);

#endif
