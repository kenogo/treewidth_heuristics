/*
 * graph.h - data structures and interfaces for graphs and tree decompositions
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRAPH_H
#define GRAPH_H

#include "util.h"

#define GRAPH_INITIAL_CAPACITY 64
#define GRAPH_READ_BUF_INITIAL_CAPACITY 1024

/*
 * For a graph with N nodes, neighbors_size = N
 * and the nodes are {0, ..., N - 1}
 *
 * Implemented with two adjacency list, one for outgoing and one for ingoing
 * edges
 */
struct DirectedGraph
{
  struct List **neighbors_in;
  struct List **neighbors_out;
  unsigned neighbors_size;
  unsigned neighbors_cap;
  unsigned num_edges;
};

/*
 * For a graph with N nodes, neighbors_size = N
 * and the nodes are {0, ..., N - 1}
 *
 * Implemented with an adjacency list
 */
struct Graph
{
  struct List **neighbors;
  unsigned neighbors_size; // number of nodes
  unsigned neighbors_cap;
  unsigned num_edges;
};

struct TreeDecomp
{
  struct Graph *graph;

  struct List **bags;
  unsigned bags_size;
  unsigned bags_cap;

  // size of largest bag minus 1
  unsigned tree_width;
  // bag contents are in range {0, ..., num_vertices - 1}
  unsigned num_vertices;
};

int
directed_graph_add_edge (struct DirectedGraph *graph, unsigned u, unsigned v);

int
directed_graph_check_edge (struct DirectedGraph const *graph, unsigned u,
                           unsigned v);

struct DirectedGraph *
directed_graph_copy (struct DirectedGraph const *graph);

void
directed_graph_destroy (struct DirectedGraph *graph);

struct DirectedGraph *
directed_graph_init (unsigned num_nodes);

int
graph_add_edge (struct Graph *graph, unsigned u, unsigned v);

unsigned
graph_add_node (struct Graph *graph);

int
graph_check_clique (struct Graph const *graph, struct List const *nodes);

int
graph_check_edge (struct Graph const *graph, unsigned u, unsigned v);

int
graph_check_node_almost_simplicial (struct Graph const *graph, unsigned u);

int
graph_check_node_simplicial (struct Graph const *graph, unsigned u);

struct Graph *
graph_copy (struct Graph const *graph);

void
graph_destroy (struct Graph *graph);

void
graph_eliminate (struct Graph *graph, unsigned u);

struct TreeDecomp *
graph_eo2td (struct Graph const *graph, struct List const *elimination_order);

struct Graph *
graph_induce_subgraph (struct Graph  const *graph, struct List const *subset);

struct Graph *
graph_init (unsigned num_nodes);

int
graph_num_missing_edges_for_clique (struct Graph const *graph,
                                    struct List const *nodes);

struct Graph *
graph_read_from_file (char const *filename);

int
graph_remove_edge (struct Graph *graph, unsigned u, unsigned v);

void
graph_separate (struct Graph *graph, struct List const *separator);

unsigned
tree_decomp_add_bag (struct TreeDecomp *td, struct List *bag);

void
tree_decomp_destroy (struct TreeDecomp *td);

unsigned
tree_decomp_find_largest_bag (struct TreeDecomp const *td);

int
tree_decomp_find_superset_neighbor_bag (struct TreeDecomp const *td,
                                        unsigned bag_index,
                                        struct List const *set);

struct TreeDecomp *
tree_decomp_init (unsigned num_nodes);

int
tree_decomp_update_tree_width (struct TreeDecomp *td);

int
tree_decomp_write_to_file (struct TreeDecomp const *td, char const *filename);

#endif
