#include <stdio.h>
#include <stdlib.h>
#include "graph.h"
#include "heuristic.h"
#include "util.h"

int
main (int argc, char *argv[])
{
  if (argc != 3)
    {
      printf ("Usage: ./sp2020 [inputfile] [outputfile]\n");
      return EXIT_FAILURE;
    }

  struct Graph *graph = graph_read_from_file (argv[1]);
  if (!graph)
    {
      fprintf (stderr, "[ERROR] Failed to read graph from file\n");
      return EXIT_FAILURE;
    }

  /* struct SearchHeuristicData *data */
  /*   = search_heuristic_data_init (max_cardinality_search, graph); */

  /* search_heuristic_run_on (data); */

  /* struct TreeDecomp *td = graph_eo2td (graph, data->elimination_order); */
  struct TreeDecomp *td = tree_decomp_init (1);
  for (unsigned u = 0; u < graph->neighbors_size; u++)
    {
      list_push_back (td->bags[0], u);
    }
  td->num_vertices = graph->neighbors_size;
  msvs (graph, td);
  tree_decomp_write_to_file (td, argv[2]);

  // search_heuristic_data_destroy (data);
  tree_decomp_destroy (td);
  graph_destroy (graph);
  return EXIT_SUCCESS;
}
