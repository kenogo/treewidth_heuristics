/*
 * maxflow.c - calculate the maximum flow of a flow network
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "graph.h"
#include "maxflow.h"
#include "util.h"

void
flow_destroy (struct Flow *flow)
{
  unsigned num_nodes = flow->network->graph->neighbors_size;
  for (int i = 0; i < num_nodes; i++)
    {
      list_destroy (flow->in[i]);
      list_destroy (flow->out[i]);
    }
  free (flow->in);
  free (flow->out);
  free (flow);
}

struct Flow *
flow_init (struct FlowNetwork const *network)
{
  struct Flow *flow = malloc (sizeof (*flow));
  if (!flow)
    {
      fprintf (stderr, "[ERROR] Failed allocating flow\n");
      exit (EXIT_FAILURE);
    }
  flow->network = network;

  unsigned num_nodes = network->graph->neighbors_size;
  flow->in = malloc (sizeof (struct List *) * num_nodes);
  flow->out = malloc (sizeof (struct List *) * num_nodes);
  if (!flow->in || !flow->out)
    {
      fprintf (stderr, "[ERROR] Failed allocating flow\n");
      exit (EXIT_FAILURE);
    }

  for (int i = 0; i < num_nodes; i++)
    {
      flow->in[i] = list_init ();
      flow->out[i] = list_init ();

      for (int j = 0; j < network->cap_in[i]->size; j++)
        list_push_back (flow->in[i], 0);
      for (int j = 0; j < network->cap_out[i]->size; j++)
        list_push_back (flow->out[i], 0);
    }

  return flow;
}

void
flow_network_destroy (struct FlowNetwork *network)
{
  unsigned num_nodes = network->graph->neighbors_size;
  directed_graph_destroy (network->graph);

  for (unsigned u = 0; u < num_nodes; u++)
    {
      list_destroy (network->cap_in[u]);
      list_destroy (network->cap_out[u]);
    }

  free (network->cap_in);
  free (network->cap_out);
  free (network);
}

struct Flow *
maxflow (struct FlowNetwork *network)
{
  struct Flow *flow = flow_init (network);
  struct ResidualNetwork *residual = residual_network_init (flow);
  struct List *path = residual_network_search_path (residual);
  while (path != NULL)
    {
      max_path_flow (residual, path);
      list_destroy (path);
      residual_network_rebuild (residual);
      path = residual_network_search_path (residual);
    }
  residual_network_destroy (residual);
  return flow;
}

/*
 * Strengthen flow allong the specified path in the residual network
 */
void
max_path_flow (struct ResidualNetwork *residual, struct List const *path)
{
  // Find the minimum capacity of any edge contained in the path
  unsigned min_cap = -1;
  for (int i = 0; i < path->size - 1; i++)
    {
      unsigned u = list_get (path, i);
      unsigned v = list_get (path, i + 1);
      int index = list_find (residual->graph->neighbors_out[u], v);
      if (index != -1)
        {
          unsigned cap = list_get (residual->cap_out[u], index);
          min_cap = (cap < min_cap) ? cap : min_cap;
          continue;  // prefer a path not taking reverse edges
        }
      index = list_find (residual->graph->neighbors_in[u], v);
      if (index != -1)
        {
          unsigned cap = list_get (residual->reverse_cap_in[u], index);
          min_cap = (cap < min_cap) ? cap : min_cap;
        }
    }

  for (int i = 0; i < path->size - 1; i++)
    {
      unsigned u = list_get (path, i);
      unsigned v = list_get (path, i + 1);
      int index_out = list_find (residual->graph->neighbors_out[u], v);
      int index_in = list_find (residual->graph->neighbors_in[v], u);
      if (index_out != -1)
        {
          unsigned flow_prev = list_get (residual->flow->out[u], index_out);
          list_set (residual->flow->out[u], index_out, flow_prev + min_cap);
          list_set (residual->flow->in[v], index_in, flow_prev + min_cap);
          continue;  // prefer a path not taking reverse edges
        }
      index_in = list_find (residual->graph->neighbors_in[u], v);
      index_out = list_find (residual->graph->neighbors_out[v], u);
      if (index_in != -1)
        {
          unsigned flow_prev = list_get (residual->flow->in[u], index_in);
          list_set (residual->flow->in[u], index_in, flow_prev - min_cap);
          list_set (residual->flow->out[v], index_out, flow_prev - min_cap);
        }
    }
}

void
residual_network_destroy (struct ResidualNetwork *residual)
{
  unsigned num_nodes = residual->graph->neighbors_size;
  for (int i = 0; i < num_nodes; i++)
    {
      list_destroy (residual->cap_in[i]);
      list_destroy (residual->cap_out[i]);
      list_destroy (residual->reverse_cap_in[i]);
      list_destroy (residual->reverse_cap_out[i]);
    }
  directed_graph_destroy (residual->graph);
  free (residual->cap_in);
  free (residual->cap_out);
  free (residual->reverse_cap_in);
  free (residual->reverse_cap_out);
  free (residual);
}

/*
 * Construct a residual network from a flow on a flow network
 */
struct ResidualNetwork *
residual_network_init (struct Flow const *flow)
{
  struct ResidualNetwork *residual = malloc (sizeof (*residual));
  if (!residual)
    {
      fprintf (stderr, "[ERROR] Failed allocating residual network\n");
      exit (EXIT_FAILURE);
    }

  residual->flow = flow;
  residual->graph = directed_graph_copy (flow->network->graph);
  residual->source = flow->network->source;
  residual->target = flow->network->target;

  unsigned num_nodes = residual->graph->neighbors_size;
  residual->cap_in = malloc (sizeof (struct List *) * num_nodes);
  residual->cap_out = malloc (sizeof (struct List *) * num_nodes);
  residual->reverse_cap_in = malloc (sizeof (struct List *) * num_nodes);
  residual->reverse_cap_out = malloc (sizeof (struct List *) * num_nodes);
  if (!residual->cap_in || !residual->cap_out || !residual->reverse_cap_in
      || !residual->reverse_cap_out)
    {
      fprintf (stderr,
               "[ERROR] Failed allocating residual network capacities\n");
      exit (EXIT_FAILURE);
    }

  for (int i = 0; i < num_nodes; i++)
    {
      residual->cap_in[i] = list_init ();
      residual->cap_out[i] = list_init ();
      residual->reverse_cap_in[i] = list_init ();
      residual->reverse_cap_out[i] = list_init ();

      for (int j = 0; j < flow->network->cap_in[i]->size; j++)
        {
          unsigned flow_ij = list_get (flow->in[i], j);
          unsigned cap_ij = list_get (flow->network->cap_in[i], j);
          unsigned cap = (flow_ij < cap_ij) ? (cap_ij - flow_ij) : 0;
          unsigned reverse_cap = (flow_ij > 0) ? flow_ij : 0;
          list_push_back (residual->cap_in[i], cap);
          list_push_back (residual->reverse_cap_in[i], reverse_cap);
        }

      for (int j = 0; j < flow->network->cap_out[i]->size; j++)
        {
          unsigned flow_ij = list_get (flow->out[i], j);
          unsigned cap_ij = list_get (flow->network->cap_out[i], j);
          unsigned cap = (flow_ij < cap_ij) ? (cap_ij - flow_ij) : 0;
          unsigned reverse_cap = (flow_ij > 0) ? flow_ij : 0;
          list_push_back (residual->cap_out[i], cap);
          list_push_back (residual->reverse_cap_out[i], reverse_cap);
        }
    }

  return residual;
}

/*
 * Update the residual network after a change to residual->flow
 */
void
residual_network_rebuild (struct ResidualNetwork *residual)
{
  unsigned num_nodes = residual->graph->neighbors_size;

  for (int i = 0; i < num_nodes; i++)
    {
      for (int j = 0; j < residual->cap_in[i]->size; j++)
        {
          unsigned flow_ij = list_get (residual->flow->in[i], j);
          unsigned cap_ij = list_get (residual->flow->network->cap_in[i], j);
          unsigned cap = (flow_ij < cap_ij) ? (cap_ij - flow_ij) : 0;
          unsigned reverse_cap = (flow_ij > 0) ? flow_ij : 0;
          list_set (residual->cap_in[i], j, cap);
          list_set (residual->reverse_cap_in[i], j, reverse_cap);
        }
      for (int j = 0; j < residual->cap_out[i]->size; j++)
        {
          unsigned flow_ij = list_get (residual->flow->out[i], j);
          unsigned cap_ij = list_get (residual->flow->network->cap_out[i], j);
          unsigned cap = (flow_ij < cap_ij) ? (cap_ij - flow_ij) : 0;
          unsigned reverse_cap = (flow_ij > 0) ? flow_ij : 0;
          list_set (residual->cap_out[i], j, cap);
          list_set (residual->reverse_cap_out[i], j, reverse_cap);
        }
    }
}

/*
 * Needed for residual_network_search_path
 *
 * It effectively turns the MaxHeap into a MinHeap (Dijkstra's is interested in
 * the smallest distance, after all)
 */
static int
_search_path_distance_less_than (void *a, void *b)
{
  unsigned x = *((unsigned *) a);
  unsigned y = *((unsigned *) b);
  return x < y;
}

/*
 * Find a path connecting source and target in the residual network with
 * Dijkstra's algorithm
 *
 * Return NULL if no path exists
 */
struct List *
residual_network_search_path (struct ResidualNetwork *residual)
{
  unsigned num_nodes = residual->graph->neighbors_size;
  unsigned *distance = malloc (sizeof (unsigned) * num_nodes);
  unsigned *parent = malloc (sizeof (unsigned) * num_nodes);
  if (!distance || !parent)
    {
      fprintf (stderr, "[ERROR] Failed allocating arrays for path search\n");
      exit (EXIT_FAILURE);
    }
  for (int i = 0; i < num_nodes; i++)
    {
      distance[i] = -1;  // "infinity" (this isn't perfect but good enough)
    }
  distance[residual->target] = 0;

  // really a MinHeap, see _search_path_distance_less_than
  struct MaxHeap *unreached = max_heap_init (_search_path_distance_less_than);
  for (unsigned i = 0; i < num_nodes; i++)
    {
      max_heap_insert (unreached, &distance[i], i);
    }

  // We search from target to source so that we can simply push back parents to
  // a list at the end and have the path in the order from source to target
  unsigned u = residual->target;
  while (unreached->size != 0)
    {
      u = max_heap_extract (unreached);

      // path found
      if (u == residual->source)
        {
          break;
        }

      // check neighbors of incoming edges (remember that we're searching in the
      // wrong direction, from target to source)
      for (int i = 0; i < residual->graph->neighbors_in[u]->size; i++)
        {
          if (list_get (residual->cap_in[u], i) == 0)
            {
              continue;
            }
          unsigned v = list_get (residual->graph->neighbors_in[u], i);
          unsigned new_distance = (distance[u] == -1) ? -1 : distance[u] + 1;
          if (new_distance < distance[v])
            {
              distance[v] = new_distance;
              parent[v] = u;
              max_heap_rebuild (unreached);
            }
        }

      // check neighbors of outgoing edges (these can be part of the path if
      // the edges have a reverse capacity associated with it)
      for (int i = 0; i < residual->graph->neighbors_out[u]->size; i++)
        {
          if (list_get (residual->reverse_cap_out[u], i) == 0)
            {
              continue;
            }
          unsigned v = list_get (residual->graph->neighbors_out[u], i);
          unsigned new_distance = (distance[u] == -1) ? -1 : distance[u] + 1;
          if (new_distance < distance[v])
            {
              distance[v] = new_distance;
              parent[v] = u;
              max_heap_rebuild (unreached);
            }
        }
    }

  // no path found
  if (u != residual->source || distance[residual->source] == -1)
    {
      free (distance);
      free (parent);
      max_heap_destroy (unreached);
      return NULL;
    }

  // trace back the path
  struct List *path = list_init ();
  list_push_back (path, u);
  while (u != residual->target)
    {
      if (parent[u] >= residual->graph->neighbors_size)
        {
          printf ("FUCK!\n");
        }
      u = parent[u];
      list_push_back (path, u);
    }

  free (distance);
  free (parent);
  max_heap_destroy (unreached);
  return path;
}
