/*
 * heuristic.c - heuristics for perfect elimination orders
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "graph.h"
#include "heuristic.h"
#include "maxflow.h"
#include "util.h"

static int
lexbfs_greater_than (void *a, void *b)
{
  struct List *x = ((struct List *) a);
  struct List *y = ((struct List *) b);
  unsigned size = (x->size < y->size) ? x->size : y->size;
  unsigned i = 0;
  while (i < size)
    {
      if (list_get (x, i) != list_get (y, i))
        break;
      i += 1;
    }
  if (i == size)
    return ((x->size < y->size) ? 0 : 1);
  return list_get (x, i) > list_get (y, i);
}

static void
lexbfs_keys_destroy (void **keys, unsigned size)
{
  for (int i = 0; i < size; i++)
    list_destroy ((struct List *) keys[i]);
  free (keys);
}

static void **
lexbfs_keys_init (unsigned size)
{
  void **keys = malloc (sizeof (void *) * size);
  if (!keys)
    {
      fprintf (stderr, "[ERROR] Failed to allocate keys for LexBFS\n");
      exit (EXIT_FAILURE);
    }
  for (int i = 0; i < size; i++)
    {
      keys[i] = (void *) list_init ();
    }
  return keys;
}

static void
lexbfs_search_next (struct Graph const *graph, struct MaxHeap *unreached,
                    struct List *elimination_order_reverse, void **keys)
{
  unsigned u = max_heap_extract (unreached);
  list_push_back (elimination_order_reverse, u);
  for (int i = 0; i < graph->neighbors[u]->size; i++)
    {
      unsigned v = list_get (graph->neighbors[u], i);
      struct List *element = ((struct List *) keys[v]);
      list_push_back (element, unreached->size);
    }
  max_heap_rebuild (unreached);
}

const struct SearchHeuristic lexbfs = { .greater_than = lexbfs_greater_than,
                                        .keys_destroy = lexbfs_keys_destroy,
                                        .keys_init = lexbfs_keys_init,
                                        .search_next = lexbfs_search_next };

static int
mcs_greater_than (void *a, void *b)
{
  unsigned x = *((unsigned *) a);
  unsigned y = *((unsigned *) b);
  return x > y;
}

static void
mcs_keys_destroy (void **keys, unsigned size)
{
  for (int i = 0; i < size; i++)
    free (keys[i]);
  free (keys);
}

static void **
mcs_keys_init (unsigned size)
{
  void **keys = malloc (sizeof (void *) * size);
  if (!keys)
    {
      fprintf (stderr, "[ERROR] Failed to allocate keys for MCS\n");
      exit (EXIT_FAILURE);
    }
  for (int i = 0; i < size; i++)
    {
      keys[i] = calloc (sizeof (unsigned), 1);
      if (!keys[i])
        {
          fprintf (stderr, "[ERROR] Failed to allocate keys for MCS\n");
          exit (EXIT_FAILURE);
        }
    }
  return keys;
}

static void
mcs_search_next (struct Graph const *graph, struct MaxHeap *unreached,
                 struct List *elimination_order_reverse, void **keys)
{
  unsigned u = max_heap_extract (unreached);
  list_push_back (elimination_order_reverse, u);
  for (int i = 0; i < graph->neighbors[u]->size; i++)
    {
      unsigned v = list_get (graph->neighbors[u], i);
      unsigned *element = ((unsigned *) keys[v]);
      *element += 1;
    }
  max_heap_rebuild (unreached);
}

const struct SearchHeuristic
max_cardinality_search = {.greater_than = mcs_greater_than,
                          .keys_destroy = mcs_keys_destroy,
                          .keys_init = mcs_keys_init,
                          .search_next = mcs_search_next };

/*
 * Get the minimum separator of source and target for the graph
 */
struct List*
min_separator (struct Graph const *graph, unsigned source, unsigned target)
{
  struct FlowNetwork *network
    = min_separator_flow_network_init (graph, source, target);
  struct Flow *flow = maxflow (network);
  struct ResidualNetwork *residual = residual_network_init (flow);

  struct List *cut = list_init ();
  for (unsigned u = 0; u < network->graph->neighbors_size; u++)
    {
      residual->target = u;
      struct List *path = residual_network_search_path (residual);
      if (path != NULL)
        {
          set_add (cut, u);
          list_destroy (path);
        }
    }

  struct List *separator = list_init ();
  for (int i = 0; i < cut->size; i++)
    {
      unsigned u = list_get (cut, i);
      for (int j = 0; j < residual->graph->neighbors_out[u]->size; j++)
        {
          unsigned v = list_get (residual->graph->neighbors_out[u], j);

          // not part of delta+(S)
          if (list_find (cut, v) != -1)
            {
              continue;
            }

          if (u >= graph->neighbors_size)  // (v-, v+)
            {
              if (v != source && v != target)
                {
                  set_add (separator, v);
                }
            }
          else  // (u+, v-)
            {
              v -= graph->neighbors_size;
              if (u != source && u != target)
                {
                  set_add (separator, u);
                }
              if (v != source && v != target)
                {
                  set_add (separator, v);
                }
            }
        }
    }

  flow_destroy (flow);
  flow_network_destroy (network);
  residual_network_destroy (residual);
  list_destroy (cut);
  return separator;
}

/*
 * Initialize a flow network based on a graph, as needed for the reduction of
 * finding the minimum separator onto the problem of finding the maximum flow
 * of the flow network
 */
struct FlowNetwork*
min_separator_flow_network_init (struct Graph const *graph, unsigned source,
                                 unsigned target)
{
  struct DirectedGraph *dgraph
    = directed_graph_init (graph->neighbors_size * 2);

  for (unsigned u = 0; u < graph->neighbors_size; u++)
    {
      for (int i = 0; i < graph->neighbors[u]->size; i++)
        {
          unsigned v = list_get (graph->neighbors[u], i);
          if (v < u)  // Don't add the same edge twice
            {
              continue;
            }
          directed_graph_add_edge (dgraph, u, v + graph->neighbors_size);
          directed_graph_add_edge (dgraph, v, u + graph->neighbors_size);
        }
      directed_graph_add_edge (dgraph, u + graph->neighbors_size, u);
    }

  struct FlowNetwork *flow_network = malloc (sizeof (*flow_network));
  if (!flow_network)
    {
      fprintf (stderr, "[ERROR] Failed to allocate flow network\n");
      exit (EXIT_FAILURE);
    }
  flow_network->graph = dgraph;

  unsigned num_nodes = flow_network->graph->neighbors_size;
  flow_network->cap_in = malloc (sizeof (struct List *) * num_nodes);
  flow_network->cap_out = malloc (sizeof (struct List *) * num_nodes);
  if (!flow_network->cap_in || !flow_network->cap_out)
    {
      fprintf (stderr, "[ERROR] Faield to allocate flow network\n");
      exit (EXIT_FAILURE);
    }

  for (unsigned u = 0; u < num_nodes; u++)
    {
      flow_network->cap_in[u] = list_init ();
      flow_network->cap_out[u] = list_init ();
    }

  for (unsigned u = 0; u < num_nodes; u++)
    {
      for (int i = 0; i < flow_network->graph->neighbors_out[u]->size; i++)
        {
          list_push_back (flow_network->cap_out[u], 1);
        }
      for (int i = 0; i < flow_network->graph->neighbors_in[u]->size; i++)
        {
          list_push_back (flow_network->cap_in[u], 1);
        }
    }

  flow_network->source = source;
  flow_network->target = target + graph->neighbors_size;
  return flow_network;
}

static void
msvs_connect_old_bag_neighbors (struct TreeDecomp const *td, unsigned bag_index,
                                struct List const *old_bag_neighbors)
{
  for (int i = 0; i < old_bag_neighbors->size; i++)
    {
      unsigned bag1_index = list_get (old_bag_neighbors, i);
      struct List *bag1 = td->bags[bag1_index];
      struct List *bag1_minus_separator = list_init ();
      list_copy (bag1_minus_separator, bag1);
      set_substract (bag1_minus_separator, td->bags[bag_index]);
      int found_match = 0;
      for (int j = 0; j < td->graph->neighbors[bag_index]->size; j++)
        {
          unsigned bag2_index = list_get (td->graph->neighbors[bag_index], j);
          struct List *bag2 = td->bags[bag2_index];
          if (set_intersects (bag1_minus_separator, bag2))
            {
              graph_add_edge (td->graph, bag1_index, bag2_index);
              found_match = 1;
              break;
            }
        }
      if (found_match == 0)
        {
          graph_add_edge (td->graph, bag1_index, bag_index);
        }
      list_destroy (bag1_minus_separator);
    }
}

static struct Graph*
msvs_get_helper (struct Graph const *graph, struct TreeDecomp const *td,
                 unsigned bag_index, int *is_clique, unsigned *source,
                 unsigned *target)
{
  struct List *bag = td->bags[bag_index];
  struct Graph *helper = graph_induce_subgraph (graph, bag);

  *is_clique = 1;
  for (unsigned i = 0; i < bag->size - 1; i++)
    {
      unsigned u = list_get (bag, i);
      for (unsigned j = i + 1; j < bag->size; j++)
        {
          unsigned v = list_get (bag, j);
          struct List *set = list_init ();
          list_push_back (set, u);
          list_push_back (set, v);
          int superset_bag_index =
            tree_decomp_find_superset_neighbor_bag (td, bag_index, set);
          list_destroy (set);
          if (superset_bag_index != -1)
            {
              graph_add_edge (helper, i, j);
            }
          else if (!graph_check_edge (helper, i, j))
            {
              *is_clique = 0;
              *source = i;
              *target = j;
            }
        }
    }

  return helper;
}

static void
msvs_replace_old_bag (struct TreeDecomp *td, unsigned bag_index,
                      struct Graph const *helper, struct List const *separator)
{
  struct List *old_bag = list_init ();
  list_copy (old_bag, td->bags[bag_index]);

  while (td->graph->neighbors[bag_index]->size > 0)
    {
      graph_remove_edge (td->graph, bag_index,
                         list_get (td->graph->neighbors[bag_index], 0));
    }
  list_destroy (td->bags[bag_index]);

  // This converts the indices of the helper graph back to those of our original
  // graph
  td->bags[bag_index] = list_get_list (old_bag, separator);

  int *visited = calloc (helper->neighbors_size, sizeof (int));
  for (int i = 0; i < separator->size; i++)
    {
      unsigned u = list_get (separator, i);
      visited[u] = 1;
    }
  struct Queue *queue = queue_init ();

  for (unsigned u = 0; u < helper->neighbors_size; u++)
    {
      if (visited[u])
        {
          continue;
        }
      visited[u] = 1;
      queue_enqueue (queue, u);
      struct List *new_bag = list_init ();
      while (!queue->size == 0)
        {
          unsigned v = queue_dequeue (queue);
          list_push_back (new_bag, v);
          for (int i = 0; i < helper->neighbors[v]->size; i++)
            {
              unsigned w = list_get (helper->neighbors[v], i);
              if (visited[w])
                {
                  continue;
                }
              visited[w] = 1;
              queue_enqueue (queue, w);
            }
        }
      list_append (new_bag, separator);
      // This converts the indices of the helper graph back to those of our
      // original graph
      struct List *new_bag_translated = list_get_list (old_bag, new_bag);
      unsigned new_bag_index = tree_decomp_add_bag (td, new_bag_translated);
      list_destroy (new_bag);
      graph_add_edge (td->graph, bag_index, new_bag_index);
    }

  list_destroy (old_bag);
  free (visited);
  queue_destroy (queue);
}

void
msvs (struct Graph const *graph, struct TreeDecomp *td)
{
  unsigned bag_index = tree_decomp_find_largest_bag (td);

  int is_clique;
  unsigned source, target;
  struct Graph *helper = msvs_get_helper (graph, td, bag_index, &is_clique,
                                          &source, &target);

  if (is_clique)  // We can't find a separator in that case
    {
      graph_destroy (helper);
      return;
    }

  struct List *separator = min_separator (helper, source, target);
  graph_separate (helper, separator);

  struct List *old_bag_neighbors = list_init ();
  list_copy (old_bag_neighbors, td->graph->neighbors[bag_index]);
  msvs_replace_old_bag (td, bag_index, helper, separator);

  msvs_connect_old_bag_neighbors (td, bag_index, old_bag_neighbors);

  graph_destroy (helper);
  list_destroy (separator);
  list_destroy (old_bag_neighbors);
  msvs (graph, td);
}

/*
 * Reduction rule for almost simplicial nodes
 */
void
reduce_almost_simplicial (struct SearchHeuristicData *data)
{
  // We use the reduction rule at the beginning, so all keys of the heap are
  // set to 0 and we don't have to worry about rebuilding the heap.
  // Consequently, it is simpler to think of the unreached nodes as a list
  struct List unreached_list = { .elements = data->unreached->values,
                                 .size = data->unreached->size,
                                 .cap = data->unreached->cap };

  unsigned lower_bound = tree_width_lower_bound (data->graph);

  // This reduction rule can't do anything with such a small lower bound
  if (lower_bound < 2)
    return;

  for (int i = 0; i < unreached_list.size; i++)
    {
      unsigned u = list_get (&unreached_list, i);
      if (lower_bound >= data->graph->neighbors[u]->size
          && graph_check_node_almost_simplicial (data->graph, u))
        {
          list_remove_at (&unreached_list, i);
          list_push_back (data->elimination_order, u);
          graph_eliminate (data->graph, u);
        }
    }

  data->unreached->size = unreached_list.size;
  data->unreached->cap = unreached_list.cap;
}

/*
 * Buddy rule
 */
void
reduce_buddy (struct SearchHeuristicData *data)
{
  // We use the reduction rule at the beginning, so all keys of the heap are
  // set to 0 and we don't have to worry about rebuilding the heap.
  // Consequently, it is simpler to think of the unreached nodes as a list
  struct List unreached_list = { .elements = data->unreached->values,
                                 .size = data->unreached->size,
                                 .cap = data->unreached->cap };

  unsigned lower_bound = tree_width_lower_bound (data->graph);

  // This reduction rule can't do anything with such a small lower bound
  if (lower_bound < 3)
    return;

  for (int i = 0; i < unreached_list.size; i++)
    {
      for (int j = i + 1; j < unreached_list.size; j++)
        {
          unsigned u = list_get (&unreached_list, i);
          unsigned v = list_get (&unreached_list, j);
          if (data->graph->neighbors[u]->size == 3
              && data->graph->neighbors[v]->size == 3
              && set_equal (data->graph->neighbors[u],
                            data->graph->neighbors[v]))
            {
              list_remove_at (&unreached_list, i);
              // j - 1 because of the removal in the previous line
              list_remove_at (&unreached_list, j - 1);
              list_push_back (data->elimination_order, u);
              list_push_back (data->elimination_order, v);
              graph_eliminate (data->graph, u);
              graph_eliminate (data->graph, v);
              continue;
            }
        }
    }

  data->unreached->size = unreached_list.size;
  data->unreached->cap = unreached_list.cap;
}

/*
 * Reduction rule for simplicial nodes
 */
void
reduce_simplicial (struct SearchHeuristicData *data)
{
  // We use the reduction rule at the beginning, so all keys of the heap are
  // set to 0 and we don't have to worry about rebuilding the heap.
  // Consequently, it is simpler to think of the unreached nodes as a list
  struct List unreached_list = { .elements = data->unreached->values,
                                 .size = data->unreached->size,
                                 .cap = data->unreached->cap };

  for (int i = 0; i < unreached_list.size; i++)
    {
      unsigned u = list_get (&unreached_list, i);
      if (graph_check_node_simplicial (data->graph, u))
        {
          list_remove_at (&unreached_list, i);
          list_push_back (data->elimination_order, u);
        }
    }

  data->unreached->size = unreached_list.size;
  data->unreached->cap = unreached_list.cap;
}

/*
 * Run a search heuristic on a graph and return the resulting elimination order
 *
 * Several search heuristics are defined as global constants here. You can
 * simply use them by passing them as the first argument - or you can create
 * your own search heuristic to pass on.
 *
 * If you want to use reduction rules, see search_heuristic_run_on.
 */
struct List *
search_heuristic_run (struct SearchHeuristic heuristic,
                      struct Graph const *graph)
{
  struct SearchHeuristicData *data
    = search_heuristic_data_init (heuristic, graph);

  search_heuristic_run_on (data);

  struct List *elimination_order = list_init ();
  list_copy (elimination_order, data->elimination_order);
  search_heuristic_data_destroy (data);
  return elimination_order;
}

/*
 * Run a search heuristic on manually initialized data (including the heap
 * of unreached nodes, the keys and the elimination_order).
 *
 * This is used when you want to use reduction rules. You can first initialize
 * the data with search_heuristic_data_init, then run a couple reduction
 * rules on it, then pass it onto this function, and run
 * search_heuristic_data_destroy when you're finished. Check
 * search_heuristic_run for reference.
 */
void
search_heuristic_run_on (struct SearchHeuristicData *data)
{
  while (data->unreached->size != 0)
    data->heuristic.search_next (data->graph, data->unreached,
                                 data->elimination_order_reverse, data->keys);
  for (int i = data->elimination_order_reverse->size - 1; i >= 0; i--)
    list_push_back (data->elimination_order,
                    list_get (data->elimination_order_reverse, i));
}

void
search_heuristic_data_destroy (struct SearchHeuristicData *data)
{
  data->heuristic.keys_destroy (data->keys, data->graph->neighbors_size);
  max_heap_destroy (data->unreached);
  list_destroy (data->elimination_order);
  list_destroy (data->elimination_order_reverse);
  graph_destroy (data->graph);
  free (data);
}

struct SearchHeuristicData *
search_heuristic_data_init (struct SearchHeuristic heuristic,
                            struct Graph const *graph)
{
  struct SearchHeuristicData *data = malloc (sizeof (*data));
  if (!data)
    {
      fprintf (stderr, "[ERROR] Failed to allocate search heuristic data\n");
      exit (EXIT_FAILURE);
    }

  data->heuristic = heuristic;
  data->graph = graph_copy (graph);
  data->keys = heuristic.keys_init (graph->neighbors_size);

  data->unreached = max_heap_init (heuristic.greater_than);
  for (unsigned u = 0; u < graph->neighbors_size; u++)
    max_heap_insert (data->unreached, data->keys[u], u);

  data->elimination_order = list_init ();
  data->elimination_order_reverse = list_init ();
  return data;
}

/*
 * Obtain a lower bound for the tree width of graph, to use for example for
 * reduction rules
 */
unsigned
tree_width_lower_bound (struct Graph const *graph)
{
  unsigned lower_bound = graph->num_edges / graph->neighbors_size;
  unsigned min_degree = -1;
  for (unsigned u = 0; u < graph->neighbors_size; u++)
    min_degree = ((graph->neighbors[u]->size < min_degree)
                  ? graph->neighbors[u]->size : min_degree);
  lower_bound = (lower_bound > min_degree) ? lower_bound : min_degree;
  return lower_bound;
}
