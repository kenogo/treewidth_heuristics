/*
 * graph.c - data structures and interfaces for graphs and tree decompositions
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "graph.h"
#include "util.h"

/*
 * Add edge (u, v) to graph
 *
 * Return values:
 * -1: u or v are not valid nodes of the graph
 * 0: (u, v) already existed in graph
 * 1: (u, v) was added to graph
 */
int
directed_graph_add_edge (struct DirectedGraph *graph, unsigned u, unsigned v)
{
  // Node indices out of bounds
  if (u >= graph->neighbors_size || v >= graph->neighbors_size)
    {
      fprintf (stderr, "[ERROR] Node indices out of bounds\n");
      return -1;
    }

  // Edge already existed in graph
  if (directed_graph_check_edge (graph, u, v) == 1)
    return 0;

  // Add edge to graph
  list_push_back (graph->neighbors_out[u], v);
  list_push_back (graph->neighbors_in[v], u);
  graph->num_edges += 1;
  return 1;
}

/*
 * Check if graph contains the edge (u, v)
 *
 * Return values:
 * -1: u or v are not valid nodes of the graph
 * 0: (u, v) is not in graph
 * 1: (u, v) is in graph
 */
int
directed_graph_check_edge (struct DirectedGraph const *graph, unsigned u,
                           unsigned v)
{
  // Node indices out of bounds
  if (u >= graph->neighbors_size || v >= graph->neighbors_size)
    {
      fprintf (stderr, "[ERROR] Node indices out of bounds\n");
      return -1;
    }

  // Perform search on the smaller adjacency list
  struct List *to_search = graph->neighbors_out[u];
  unsigned to_find = v;
  if (graph->neighbors_out[u]->size > graph->neighbors_in[v]->size)
    {
      to_search = graph->neighbors_in[v];
      to_find = u;
    }

  if (list_find (to_search, to_find) != -1)
    return 1;
  return 0;
}

struct DirectedGraph *
directed_graph_copy (struct DirectedGraph const *graph)
{
  unsigned num_nodes = graph->neighbors_size;
  struct DirectedGraph *copy = directed_graph_init (num_nodes);
  copy->num_edges = graph->num_edges;

  for (int i = 0; i < num_nodes; i++)
    {
      list_copy (copy->neighbors_in[i], graph->neighbors_in[i]);
      list_copy (copy->neighbors_out[i], graph->neighbors_out[i]);
    }

  return copy;
}

void
directed_graph_destroy (struct DirectedGraph *graph)
{
  for (int i = 0; i < graph->neighbors_size; i++)
    {
      list_destroy (graph->neighbors_in[i]);
      list_destroy (graph->neighbors_out[i]);
    }
  free (graph->neighbors_in);
  free (graph->neighbors_out);
  free (graph);
}

struct DirectedGraph *
directed_graph_init (unsigned num_nodes)
{
  struct DirectedGraph *graph = malloc (sizeof (*graph));
  if (graph == NULL)
    {
      fprintf (stderr, "[ERROR] Failed to allocate graph\n");
      exit (EXIT_FAILURE);
    }

  graph->neighbors_size = num_nodes;
  graph->neighbors_cap = (num_nodes > GRAPH_INITIAL_CAPACITY
                          ? num_nodes : GRAPH_INITIAL_CAPACITY);
  graph->num_edges = 0;

  // Allocate adjacency lists
  graph->neighbors_in = malloc (sizeof (struct List *) * graph->neighbors_cap);
  graph->neighbors_out = malloc (sizeof (struct List *) * graph->neighbors_cap);
  if (!graph->neighbors_in || !graph->neighbors_out)
    {
      fprintf (stderr, "[ERROR] Failed to allocate adjacency list\n");
      exit (EXIT_FAILURE);
    }

  // Allocate rows of adjacency list
  for (int i = 0; i < graph->neighbors_size; i++)
    {
      graph->neighbors_in[i] = list_init ();
      graph->neighbors_out[i] = list_init ();
    }

  return graph;
}

/*
 * Add edge (u, v) to graph
 *
 * Return values:
 * -1: u or v are not valid nodes of the graph
 * 0: (u, v) already existed in graph
 * 1: (u, v) was added to graph
 */
int
graph_add_edge (struct Graph *graph, unsigned u, unsigned v)
{
  // Node indices out of bounds
  if (u >= graph->neighbors_size || v >= graph->neighbors_size)
    {
      fprintf (stderr, "[ERROR] Node indices out of bounds\n");
      return -1;
    }

  // Edge already existed in graph
  if (graph_check_edge (graph, u, v) == 1)
    return 0;

  // Add edge to graph
  list_push_back (graph->neighbors[u], v);
  list_push_back (graph->neighbors[v], u);
  graph->num_edges += 1;
  return 1;
}

/*
 * Add a node to the graph and return its index
 */
unsigned
graph_add_node (struct Graph *graph)
{
  if (graph->neighbors_size >= graph->neighbors_cap)
    {
      graph->neighbors_cap *= 2;
      graph->neighbors =
        realloc (graph->neighbors,
                 sizeof (struct List *) * graph->neighbors_cap);
      if (!graph->neighbors)
        {
          fprintf (stderr, "[ERROR] Failed to add node\n");
          exit (EXIT_FAILURE);
        }
    }
  graph->neighbors[graph->neighbors_size] = list_init ();
  graph->neighbors_size += 1;
  return graph->neighbors_size - 1;
}

/*
 * Check if nodes form a clique in graph
 *
 * Return values:
 * -1: At least one of the nodes is not a valid node of the graph
 * 0: nodes are not a clique in graph
 * 1: nodes are a clique in graph
 */
int
graph_check_clique (struct Graph const *graph, struct List const *nodes)
{
  int num = graph_num_missing_edges_for_clique (graph, nodes);
  if (num == -1)
    return -1;
  return (num == 0);
}

/*
 * Check if graph contains the edge (u, v)
 *
 * Return values:
 * -1: u or v are not valid nodes of the graph
 * 0: (u, v) is not in graph
 * 1: (u, v) is in graph
 */
int
graph_check_edge (struct Graph const *graph, unsigned u, unsigned v)
{
  // Node indices out of bounds
  if (u >= graph->neighbors_size || v >= graph->neighbors_size)
    {
      fprintf (stderr, "[ERROR] Node indices out of bounds\n");
      return -1;
    }

  // Perform search on the smaller adjacency list
  unsigned utmp = u;
  if (graph->neighbors[u]->size > graph->neighbors[v]->size)
    {
      u = v;
      v = utmp;
    }

  if (list_find (graph->neighbors[u], v) != -1)
    return 1;
  return 0;
}

/*
 * Check if node u is almost simplicial
 *
 * Return values:
 * -1: u is not a valid node of the graph
 * 0: u is not almost simplicial
 * 1: u is almost simplicial
 */
int
graph_check_node_almost_simplicial (struct Graph const *graph, unsigned u)
{
  struct List *neighbors_u = graph->neighbors[u];
  int num = graph_num_missing_edges_for_clique (graph, neighbors_u);
  if (num == -1)
    return -1;
  return (num == 1);
}

/*
 * Check if node u is simplicial
 *
 * Return values:
 * -1: u is not a valid node of the graph
 * 0: u is not simplicial
 * 1: u is simplicial
 */
int
graph_check_node_simplicial (struct Graph const *graph, unsigned u)
{
  struct List *neighbors_u = graph->neighbors[u];
  return graph_check_clique (graph, neighbors_u);
}

/*
 * Create a deep copy of a graph
 */
struct Graph *
graph_copy (struct Graph const *graph)
{
  unsigned num_nodes = graph->neighbors_size;
  struct Graph *copy = graph_init (num_nodes);
  copy->num_edges = graph->num_edges;

  for (int i = 0; i < num_nodes; i++)
    list_copy (copy->neighbors[i], graph->neighbors[i]);

  return copy;
}

void
graph_destroy (struct Graph *graph)
{
  for (int i = 0; i < graph->neighbors_size; i++)
    list_destroy (graph->neighbors[i]);
  free (graph->neighbors);
  free (graph);
}

/*
 * Isolate u and turn its neighbors into a clique
 */
void
graph_eliminate (struct Graph *graph, unsigned u)
{
  struct List *neighbors_u = graph->neighbors[u];
  for (int i = 0; i < neighbors_u->size; i++)
    {
      for (int j = i + 1; j < neighbors_u->size; j++)
        {
          unsigned v = list_get (neighbors_u, i);
          unsigned w = list_get (neighbors_u, j);
          graph_add_edge (graph, v, w);
        }
    }

  while (neighbors_u->size > 0)
    graph_remove_edge (graph, u, list_get (neighbors_u, 0));
}

struct TreeDecomp *
graph_eo2td (struct Graph const *graph, struct List const *elimination_order)
{
  unsigned num_nodes = graph->neighbors_size;
  struct TreeDecomp *td = tree_decomp_init (num_nodes);
  td->num_vertices = num_nodes;
  struct Graph *triang = graph_copy (graph);    // Triangulation

  // list_get (elimination_order, i) = u
  // loookup[u] = i
  unsigned *lookup = malloc (sizeof (unsigned) * num_nodes);
  if (!lookup)
    {
      fprintf (stderr, "[ERROR] Failed to allocate lookup table");
    }

  // Initialize bags and reverse EO lookup
  for (int i = 0; i < num_nodes; i++)
    {
      unsigned u = list_get (elimination_order, i);
      lookup[u] = i;
      set_add (td->bags[i], u);
      set_union (td->bags[i], graph->neighbors[u]);
    }

  // Eliminate node u
  for (int i = 0; i < num_nodes; i++)
    {
      unsigned u = list_get (elimination_order, i);

      // delete u from bags of nodes that are later in EO
      for (int j = 0; j < triang->neighbors[u]->size; j++)
        {
          unsigned v = list_get (triang->neighbors[u], j);
          unsigned k = lookup[v];
          if (k > i)
            list_remove (td->bags[k], u);
        }

      // make clique
      for (int j = 0; j < triang->neighbors[u]->size; j++)
        {
          for (int k = j + 1; k < triang->neighbors[u]->size; k++)
            {
              unsigned v = list_get (triang->neighbors[u], j);
              unsigned w = list_get (triang->neighbors[u], k);
              unsigned l = lookup[v];
              unsigned m = lookup[w];
              if (l <= i || m <= i)
                continue;
              int ret = graph_add_edge (triang, v, w);
              if (ret == 1)
                {
                  set_add (td->bags[l], w);
                  set_add (td->bags[m], v);
                }
            }
        }

      // connect bag of u to nearest neighbor in EO
      unsigned min = -1;
      for (int j = 0; j < triang->neighbors[u]->size; j++)
        {
          unsigned v = list_get (triang->neighbors[u], j);
          unsigned k = lookup[v];
          min = (k < min && k > i) ? k : min;
        }
      if (min < triang->neighbors_size)
        graph_add_edge (td->graph, i, min);
      else if (i < triang->neighbors_size - 1)
        graph_add_edge (td->graph, i, i + 1);
    }

  free (lookup);
  graph_destroy (triang);
  tree_decomp_update_tree_width (td);
  return td;
}

/*
 * Return the graph induced from graph by the nodes in subset. The nodes are
 * indexed precisely like in subset.
 */
struct Graph *
graph_induce_subgraph (struct Graph const *graph, struct List const *subset)
{
  struct Graph *induced = graph_init (subset->size);

  for (int i = 0; i < subset->size; i++)
    {
      unsigned u = list_get (subset, i);
      for (int j = 0; j < graph->neighbors[u]->size; j++)
        {
          unsigned v = list_get (graph->neighbors[u], j);
          if (v < u)  // Don't add the same edge twice
            {
              continue;
            }
          int v_in_subset = list_find (subset, v);
          if (v_in_subset != -1)
            {
              graph_add_edge (induced, i, v_in_subset);
            }
        }
    }

  return induced;
}

struct Graph *
graph_init (unsigned num_nodes)
{
  struct Graph *graph = malloc (sizeof (*graph));
  if (graph == NULL)
    {
      fprintf (stderr, "[ERROR] Failed to allocate graph\n");
      exit (EXIT_FAILURE);
    }

  graph->neighbors_size = num_nodes;
  graph->neighbors_cap = (num_nodes > GRAPH_INITIAL_CAPACITY
                          ? num_nodes : GRAPH_INITIAL_CAPACITY);
  graph->num_edges = 0;

  // Allocate adjacency list
  graph->neighbors = malloc (sizeof (struct List *) * graph->neighbors_cap);
  if (!graph->neighbors)
    {
      fprintf (stderr, "[ERROR] Failed to allocate adjacency list\n");
      exit (EXIT_FAILURE);
    }

  // Allocate rows of adjacency list
  for (int i = 0; i < graph->neighbors_size; i++)
    graph->neighbors[i] = list_init ();

  return graph;
}

/*
 * Return the number of missing edges for nodes to be a clique in graph.
 * Return -1 if at least one of the nodes is not in graph.
 */
int
graph_num_missing_edges_for_clique (struct Graph const *graph,
                                    struct List const *nodes)
{
  int num = 0;
  for (int i = 0; i < nodes->size; i++)
    {
      for (int j = i + 1; j < nodes->size; j++)
        {
          unsigned u = list_get (nodes, i);
          unsigned v = list_get (nodes, j);
          int ret = graph_check_edge (graph, u, v);
          if (ret == -1)
            return -1;
          num = (ret == 0) ? num + 1 : num;
        }
    }
  return num;
}

/*
 * Read a graph from a file with the format specified here:
 * https://pacechallenge.org/2017/treewidth/
 *
 * Returns NULL if the file doesn't follow the format specification.
 */
struct Graph *
graph_read_from_file (char const *filename)
{
  struct Graph *graph;
  FILE *fp = fopen (filename, "r");
  if (!fp)
    {
      fprintf (stderr, "[ERROR] Could not open input file\n");
      return NULL;
    }

  int got_header = 0;           // File header needs to be read before data
  char *buf = malloc (GRAPH_READ_BUF_INITIAL_CAPACITY);
  size_t buf_size = GRAPH_READ_BUF_INITIAL_CAPACITY;

  while (readline (&buf, &buf_size, fp))
    {
      // Line is a comment
      if (buf[0] == 'c')
        continue;

      // Read file header
      if (!got_header)
        {
          unsigned num_edges, num_nodes;
          int ret = sscanf (buf, "p tw %u %u", &num_nodes, &num_edges);
          if (ret != 2)
            {
              fprintf (stderr, "[ERROR] Corrupted file header\n");
              return NULL;
            }
          graph = graph_init (num_nodes);
          got_header = 1;
          continue;
        }

      // Read edges
      unsigned u, v;
      int ret = sscanf (buf, "%u %u", &u, &v);
      if (ret != 2)
        {
          fprintf (stderr, "[ERROR] Corrupted file data\n");
          graph_destroy (graph);
          return NULL;
        }

      // Switch to 0-based indexing
      graph_add_edge (graph, --u, --v);
    }

  free (buf);
  fclose (fp);
  return graph;
}

/*
 * Remove edge (u, v) from graph
 *
 * Return values:
 * -1: u or v are not valid nodes of the graph
 * 0: (u, v) didn't exist in graph to begin with
 * 1: (u, v) was removed from graph
 */
int
graph_remove_edge (struct Graph *graph, unsigned u, unsigned v)
{
  // Node indices out of bounds
  if (u >= graph->neighbors_size || v >= graph->neighbors_size)
    {
      fprintf (stderr, "[ERROR] Node indices out of bounds\n");
      return -1;
    }

  if (list_remove (graph->neighbors[u], v) == -1)
    return 0;
  if (list_remove (graph->neighbors[v], u) == -1)
    return 0;

  graph->num_edges -= 1;
  return 1;
}

/*
 * Apply the separator to the graph
 */
void
graph_separate (struct Graph *graph, struct List const *separator)
{
  // Isolate the elements of separator from the rest of the graph
  for (int i = 0; i < separator->size; i++)
    {
      unsigned u = list_get (separator, i);
      for (int j = 0; j < graph->neighbors[u]->size; j++)
        {
          unsigned v = list_get (graph->neighbors[u], j);
          graph_remove_edge (graph, u, v);
        }
    }
}

/*
 * Add the bag to the tree decomposition and return its index
 */
unsigned
tree_decomp_add_bag (struct TreeDecomp *td, struct List *bag)
{
  graph_add_node (td->graph);  // TODO
  if (td->bags_size >= td->bags_cap)
    {
      td->bags_cap *= 2;
      td->bags = realloc (td->bags, sizeof (struct List *) * td->bags_cap);
      if (!td->bags)
        {
          fprintf (stderr, "[ERROR] Failed to add bag\n");
          exit (EXIT_FAILURE);
        }
    }
  td->bags[td->bags_size] = bag;
  td->bags_size += 1;
  tree_decomp_update_tree_width (td);
  return td->bags_size - 1;
}

void
tree_decomp_destroy (struct TreeDecomp *td)
{
  graph_destroy (td->graph);
  for (int i = 0; i < td->bags_size; i++)
    list_destroy (td->bags[i]);
  free (td->bags);
  free (td);
}

/*
 * Return the index of the largest bag in the tree decomposition
 */
unsigned
tree_decomp_find_largest_bag (struct TreeDecomp const *td)
{
  unsigned largest = 0;
  for (int i = 0; i < td->bags_size; i++)
    {
      largest = (td->bags[i]->size > td->bags[largest]->size) ? i : largest;
    }
  return largest;
}

/*
 * Find a bag that is a neighbor of the bag with index bag_index and a superset
 * of set and return it. Return -1 if no such bag was found.
 */
int
tree_decomp_find_superset_neighbor_bag (struct TreeDecomp const *td,
                                        unsigned bag_index,
                                        struct List const *set)
{
  for (int i = 0; i < td->graph->neighbors[bag_index]->size; i++)
    {
      unsigned neighbor_bag_index =
        list_get (td->graph->neighbors[bag_index],i);
      struct List *neighbor_bag = td->bags[neighbor_bag_index];
      if (set_is_superset (neighbor_bag, set))
        {
          return i;
        }
    }
  return -1;
}

struct TreeDecomp *
tree_decomp_init (unsigned num_nodes)
{
  struct TreeDecomp *td = malloc (sizeof (*td));
  if (td == NULL)
    {
      fprintf (stderr, "[ERROR] Failed to allocate tree decomposition\n");
      exit (EXIT_FAILURE);
    }

  td->graph = graph_init (num_nodes);

  td->bags_size = num_nodes;
  td->bags_cap = (num_nodes > GRAPH_INITIAL_CAPACITY
                  ? num_nodes : GRAPH_INITIAL_CAPACITY);

  // Allocate bags
  td->bags = malloc (sizeof (struct List *) * td->bags_cap);
  if (!td->bags)
    {
      fprintf (stderr, "[ERROR] Failed to allocate bags\n");
      exit (EXIT_FAILURE);
    }

  // Allocate individual bags
  for (int i = 0; i < td->bags_size; i++)
    td->bags[i] = list_init ();

  td->tree_width = 0;
  td->num_vertices = 0;

  return td;
}

/*
 * Update the treewidth by finding the largest bag and substracting 1 from its
 * size
 *
 * Return values:
 * 0: Treewidth unchanged
 * 1: Treewidth updated
 */
int
tree_decomp_update_tree_width (struct TreeDecomp *td)
{
  unsigned max = 0;
  for (int i = 0; i < td->bags_size; i++)
    max = (td->bags[i]->size > max) ? td->bags[i]->size : max;

  if (td->tree_width == max - 1)
    return 0;
  td->tree_width = max - 1;
  return 1;
}

/*
 * Write tree decomposition to a file with the format specified here:
 * https://pacechallenge.org/2017/treewidth/
 *
 * Return values:
 * 1: Success
 * 0: Failure
 */
int
tree_decomp_write_to_file (struct TreeDecomp const *td, char const *filename)
{
  FILE *fp = fopen (filename, "w+");
  if (!fp)
    {
      fprintf (stderr, "[ERROR] Failed to create output file\n");
      return 0;
    }

  // File header
  fprintf (fp, "s td %u %u %u\n", td->bags_size, td->tree_width + 1,
           td->num_vertices);

  // bags
  for (int i = 0; i < td->bags_size; i++)
    {
      fprintf (fp, "b %u", i + 1);
      for (int j = 0; j < td->bags[i]->size; j++)
        fprintf (fp, " %u", list_get (td->bags[i], j) + 1);
      fprintf (fp, "\n");
    }

  // edges
  for (int u = 0; u < td->graph->neighbors_size; u++)
    {
      for (int i = 0; i < td->graph->neighbors[u]->size; i++)
        {
          unsigned v = list_get (td->graph->neighbors[u], i);
          // Only print edges once
          if (u > v)
            continue;
          fprintf (fp, "%u %u\n", u + 1, v + 1);
        }
    }

  fclose (fp);
  return 1;
}
