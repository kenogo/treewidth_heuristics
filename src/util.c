/*
 * util.h - utilties like lists and heaps
 * Copyright (C) 2020  Keno Goertz

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "util.h"

/*
 * Append list2 to list1
 */
void
list_append (struct List *list1, struct List const *list2)
{
  for (int i = 0; i < list2->size; i++)
    list_push_back (list1, list_get (list2, i));
}

/*
 * Create deep copy of source in target
 */
void
list_copy (struct List *target, struct List const *source)
{
  for (int i = 0; i < source->size; i++)
    list_push_back (target, list_get (source, i));
}

void
list_destroy (struct List *list)
{
  free (list->elements);
  free (list);
}

/*
 * Find the position of the first occurance of value in the list and return it
 * Return -1 if value is not in the list
 */
int
list_find (struct List const *list, unsigned value)
{
  for (int i = 0; i < list->size; i++)
    {
      if (list_get (list, i) == value)
        return i;
    }
  return -1;
}

/*
 * Return element in list at pos
 */
unsigned
list_get (struct List const *list, unsigned pos)
{
  return list->elements[pos];
}

/*
 * Get a list of the values of list at the specified indices
 */
struct List *
list_get_list (struct List const *list, struct List const *indices)
{
  struct List *translated = list_init ();
  for (int i = 0; i < indices->size; i++)
    {
      int j = list_get (indices, i);
      list_push_back (translated, list_get (list, j));
    }
  return translated;
}

struct List *
list_init ()
{
  struct List *list = malloc (sizeof (*list));
  if (!list)
    {
      fprintf (stderr, "[ERROR] Failed to allocate list\n");
      exit (EXIT_FAILURE);
    }

  list->cap = LIST_INITIAL_CAPACITY;
  list->size = 0;
  list->elements = malloc (sizeof (unsigned) * list->cap);
  if (!list->elements)
    {
      fprintf (stderr, "[ERROR] Failed to allocate list\n");
      exit (EXIT_FAILURE);
    }

  return list;
}

/*
 * Return the maximum value in the list
 */
unsigned
list_max (struct List const *list)
{
  unsigned max = 0;
  for (int i = 0; i < list->size; i++)
    max = (list_get (list, i) > max) ? list_get (list, i) : max;
  return max;
}

/*
 * Add value to the end of list and return the position of the added element
 */
unsigned
list_push_back (struct List *list, unsigned value)
{
  if (list->size >= list->cap)
    {
      list->cap *= 2;
      list->elements =
        realloc (list->elements, sizeof (unsigned) * list->cap);
      if (!list->elements)
        {
          fprintf (stderr, "[ERROR] Failed to increase list size\n");
          exit (EXIT_FAILURE);
        }
    }
  list_set (list, list->size, value);
  list->size += 1;
  return list->size - 1;
}

/*
 * Remove the first occurance of element from the list and return the position
 * of the element before removal. If the element is not part of the list,
 * return -1.
 */
int
list_remove (struct List *list, unsigned element)
{
  int pos = list_find (list, element);
  if (pos != -1)
    list_remove_at (list, pos);
  return pos;
}

/*
 * Remove the element at pos from the list and shrink it accordingly
 *
 * Return values:
 * 0: Position out of bounds
 * 1: Element has been removed
 */
int
list_remove_at (struct List *list, unsigned pos)
{
  if (pos >= list->size)
    {
      fprintf (stderr, "[ELEMENT] Element to remove is out of bounds\n");
      return 0;
    }

  list->size -= 1;
  for (int i = pos; i < list->size; i++)
    list_set (list, i, list_get (list, i + 1));

  return 1;
}

/*
 * Reverse the order of the elements in list
 */
void
list_reverse (struct List *list)
{
  struct List *copy = list_init ();
  list_copy (copy, list);
  for (int i = 0; i < list->size; i++)
    list_set (list, i, list_get (copy, list->size - i - 1));
  list_destroy (copy);
}

/*
 * Set element at pos in list to value
 */
void
list_set (struct List *list, unsigned pos, unsigned value)
{
  list->elements[pos] = value;
}

/*
 * Read a line from a file and write it into buf, resizing it if necessary
 *
 * Return values:
 * 0: EOF
 * 1: Line has been read into buf
 */
int
readline (char **buf, size_t *size, FILE * fp)
{
  size_t offset = 0;

  while (1)
    {
      // Reached EOF, we're done!
      if (fgets (*buf + offset, *size, fp) == NULL)
        {
          *size += offset;
          return 0;
        }

      // Replace newline character with null character
      for (int i = 0; i < *size; i++)
        {
          if ((*buf + offset)[i] == '\n')
            {
              (*buf + offset)[i] = '\0';
              *size += offset;
              return 1;
            }
        }

      // No newline character in buf: We need to make more space
      offset += *size - 1;
      *size *= 2;
      *buf = realloc (*buf, *size + offset);
      if (!buf)
        {
          fprintf (stderr, "[ERROR] Failed to resize buffer to read line\n");
          exit (EXIT_FAILURE);
        }
    }
}

void
max_heap_destroy (struct MaxHeap *heap)
{
  free (heap->keys_ptrs);
  free (heap->values);
  free (heap);
}

/*
 * Extract the value with the maximum key from the heap and restore its max heap
 * property, return -1 if the heap is empty
 */
int
max_heap_extract (struct MaxHeap *heap)
{
  unsigned max_val = heap->values[0];

  if (heap->size == 0)
    return -1;

  heap->size -= 1;
  heap->keys_ptrs[0] = heap->keys_ptrs[heap->size];
  heap->values[0] = heap->values[heap->size];

  max_heap_sift_down (heap, 0);

  return max_val;
}

struct MaxHeap *
max_heap_init (int (*greater_than) (void *a, void *b))
{
  struct MaxHeap *heap = malloc (sizeof (*heap));
  if (!heap)
    {
      fprintf (stderr, "[ERROR] Failed to allocate Max Heap\n");
      exit (EXIT_FAILURE);
    }

  heap->greater_than = greater_than;
  heap->keys_ptrs = malloc (sizeof (void *) * MAX_HEAP_INITIAL_CAPACITY);
  heap->values = malloc (sizeof (unsigned) * MAX_HEAP_INITIAL_CAPACITY);
  if (!heap->keys_ptrs || !heap->values)
    {
      fprintf (stderr, "[ERROR] Failed to allocate Max Heap\n");
      exit (EXIT_FAILURE);
    }

  heap->size = 0;
  heap->cap = MAX_HEAP_INITIAL_CAPACITY;
  return heap;
}

/*
 * Insert a key-value pair into a max heap, restoring its max heap property
 * after the insertion
 */
void
max_heap_insert (struct MaxHeap *heap, void *key_ptr, unsigned value)
{
  if (heap->size >= heap->cap)
    {
      heap->cap *= 2;
      heap->keys_ptrs =
        realloc (heap->keys_ptrs, sizeof (void *) * heap->cap);
      heap->values = realloc (heap->values, sizeof (unsigned) * heap->cap);
      if (!heap->keys_ptrs || !heap->values)
        {
          fprintf (stderr, "[ERROR] Failed to reallocate Max Heap\n");
        }
    }
  heap->keys_ptrs[heap->size] = key_ptr;
  heap->values[heap->size] = value;
  heap->size += 1;

  max_heap_sift_up (heap, heap->size - 1);
}

/*
 * Restore the heap property (for example after keys have been updated)
 */
void
max_heap_rebuild (struct MaxHeap *heap)
{
  for (int i = heap->size / 2 - 1; i >= 0; i--)
    max_heap_sift_down (heap, i);
}

/*
 * Move node at pos down in the heap until the heap property is restored
 */
void
max_heap_sift_down (struct MaxHeap *heap, unsigned pos)
{
  while (1)
    {
      unsigned child1_pos = 2 * pos + 1;
      unsigned child2_pos = 2 * pos + 2;
      unsigned child_pos;

      if (child1_pos >= heap->size)
        {
          break;
        }
      else if (child2_pos >= heap->size)
        {
          child_pos = child1_pos;
        }
      else
        {
          child_pos = (heap->greater_than (heap->keys_ptrs[child1_pos],
                                           heap->keys_ptrs[child2_pos])
                       ? child1_pos : child2_pos);
        }

      void *node_key_ptr = heap->keys_ptrs[pos];
      void *child_key_ptr = heap->keys_ptrs[child_pos];

      if (heap->greater_than (node_key_ptr, child_key_ptr))
        break;

      unsigned node_val = heap->values[pos];
      unsigned child_val = heap->values[child_pos];

      heap->keys_ptrs[pos] = child_key_ptr;
      heap->keys_ptrs[child_pos] = node_key_ptr;
      heap->values[pos] = child_val;
      heap->values[child_pos] = node_val;
      pos = child_pos;
    }
}

/*
 * Move node at pos up in the heap until the heap property is restored
 */
void
max_heap_sift_up (struct MaxHeap *heap, unsigned pos)
{
  while (1)
    {
      if (pos == 0)
        break;
      unsigned parent_pos = (pos - 1) / 2;
      void *parent_key_ptr = heap->keys_ptrs[parent_pos];
      void *node_key_ptr = heap->keys_ptrs[pos];
      if (heap->greater_than (parent_key_ptr, node_key_ptr))
        break;
      unsigned parent_val = heap->values[parent_pos];
      unsigned node_val = heap->values[pos];
      heap->keys_ptrs[pos] = parent_key_ptr;
      heap->keys_ptrs[parent_pos] = node_key_ptr;
      heap->values[pos] = parent_val;
      heap->values[parent_pos] = node_val;
      pos = parent_pos;
    }
}

/*
 * Get the front element of the queue
 *
 * Return -1 if the queue is empty
 */
int
queue_dequeue (struct Queue *queue)
{
  if (queue->size == 0)
    return -1;

  int value = queue->elements[queue->current_pos];
  queue->current_pos = (queue->current_pos + 1) % queue->cap;
  queue->size -= 1;
  return value;
}

void
queue_destroy (struct Queue *queue)
{
  free (queue->elements);
  free (queue);
}

/*
 * Add an element to the end of the queue. Resize if necessary
 */
void
queue_enqueue (struct Queue *queue, unsigned value)
{
  if (queue->size >= queue->cap)
    {
      unsigned cap_prev = queue->cap;
      queue->cap *= 2;
      queue->elements =
        realloc (queue->elements, sizeof (unsigned) * queue->cap);
      if (!queue->elements)
        {
          fprintf (stderr, "[ERROR] Failed to increase queue size\n");
          exit (EXIT_FAILURE);
        }

      for (int i = 0; i < queue->current_pos; i++)
        queue->elements[i + cap_prev] = queue->elements[i];
    }

  queue->elements[(queue->current_pos + queue->size) % queue->cap] = value;
  queue->size += 1;
}

struct Queue *
queue_init ()
{
  struct Queue *queue = malloc (sizeof (*queue));
  if (!queue)
    {
      fprintf (stderr, "[ERROR] Failed to allocate queue\n");
      exit (EXIT_FAILURE);
    }

  queue->cap = QUEUE_INITIAL_CAPACITY;
  queue->size = 0;
  queue->elements = malloc (sizeof (unsigned) * queue->cap);
  if (!queue->elements)
    {
      fprintf (stderr, "[ERROR] Failed to allocate queue\n");
      exit (EXIT_FAILURE);
    }
  queue->current_pos = 0;

  return queue;
}

/*
 * Add value to set and return the position of the added value.
 * Return -1 if the value was already in the set
 */
int
set_add (struct List *set, unsigned value)
{
  if (list_find (set, value) == -1)
    return list_push_back (set, value);
  return -1;
}

/*
 * Return whether the sets are equal
 */
int
set_equal (struct List const *set1, struct List const *set2)
{
  if (set1->size != set2->size)
    return 0;

  for (int i = 0; i < set1->size; i++)
    if (list_find (set2, list_get (set1, i)) == -1)
      return 0;

  return 1;
}

/*
 * Return whether set1 and set2 intersect
 */
int
set_intersects (struct List const *set1, struct List const *set2)
{
  for (int i =0; i < set1->size; i++)
    {
      unsigned u = list_get (set1, i);
      if (list_find (set2, u) != -1)
        {
          return 1;
        }
    }
  return 0;
}

/*
 * Return whether set1 is a superset of set2
 */
int
set_is_superset (struct List const *set1, struct List const *set2)
{
  for (int i = 0; i < set2->size; i++)
    {
      unsigned u = list_get (set2, i);
      if (list_find (set1, u) == -1)
        {
          return 0;
        }
    }
  return 1;
}

/*
 * Substract set2 from set1.
 * Return the number of elements that were removed.
 */
unsigned
set_substract (struct List *set1, struct List const *set2)
{
  unsigned count = 0;
  for (int i = 0; i < set2->size; i++)
    {
      list_remove (set1, list_get (set2, i));
      count += 1;
    }
  return count;
}

/*
 * Create union of set1 and set2 and store in set1.
 * Return the numbr of elements that were added.
 */
unsigned
set_union (struct List *set1, struct List const *set2)
{
  unsigned count = 0;
  for (int i = 0; i < set2->size; i++)
    {
      if (set_add (set1, list_get (set2, i)) != -1)
        count += 1;
    }
  return count;
}
